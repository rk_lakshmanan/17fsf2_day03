//Load libraries
var express = require('express')

//Create instance of express application
var app = express();

//Route
app.get('/time',function(req,resp){
    resp.status(404);
    resp.type('text/plain');
    resp.send(new Date());
})

app.get("/someHtml",function(req,resp){
    resp.type("text/html");
    resp.status(200);
    resp.send("<h1>random h1 text</h1>");
})

//Route
app.use(express.static(__dirname+"/public"));
//remapping request for images to pics folder
app.use("/images",express.static(__dirname+"/pics"));
//this allows us to use another name for our default html instead of index.html
app.get('/',function(req,resp){
   resp.sendFile("random.html",{root:__dirname+"/public"});
})


// app.use(express.static('../client/', {index: 'login.html'}))

//Listening to this port
var port = 3000
app.listen(port,function(){
    console.log("Application is running at port %d",port);
})